
REGISTRY_HOST=$2
WORKING_DIR=$(dirname "$0")
INSTALL_DIR="$WORKING_DIR/../postgres-for-kubernetes-v1.0.0"

PROJECT="tanzu-sql"
REGISTRY="$REGISTRY_HOST/${PROJECT}"
echo ">>>>>$REGISTRY"
docker load -i $INSTALL_DIR/images/postgres-instance
docker load -i $INSTALL_DIR/images/postgres-operator

INSTANCE_IMAGE_NAME="${REGISTRY}/postgres-instance:$(cat $INSTALL_DIR/images/postgres-instance-tag)"
docker tag $(cat $INSTALL_DIR/images/postgres-instance-id) ${INSTANCE_IMAGE_NAME}
docker push ${INSTANCE_IMAGE_NAME}

OPERATOR_IMAGE_NAME="${REGISTRY}/postgres-operator:$(cat $INSTALL_DIR/images/postgres-operator-tag)"
docker tag $(cat $INSTALL_DIR/images/postgres-operator-id) ${OPERATOR_IMAGE_NAME}
docker push ${OPERATOR_IMAGE_NAME}

echo  operatorImageRepository: ${REGISTRY}/postgres-operator > "$WORKING_DIR/values.yaml"
echo  postgresImageRepository: ${REGISTRY}/postgres-instance >> "$WORKING_DIR/values.yaml"

helm install postgres-operator -f $WORKING_DIR/values.yaml $INSTALL_DIR/operator/
